<!DOCTYPE html>
<html lang="en">
  <head>
    <title>SMK PUI JATINANGOR</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.html">SMK PUI JATINANGOR</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	        	<li class="nav-item active"><a href="index.html" class="nav-link">Home</a></li>
	        	<li class="nav-item"><a href="#" class="nav-link">Jurusan</a></li>
	        	<li class="nav-item"><a href="#" class="nav-link">Kurikulum</a></li>
	        	<li class="nav-item"><a href="#" class="nav-link">Dewan Guru</a></li>
	        	<li class="nav-item"><a href="# class="nav-link">Blog</a></li>
	         	<li class="nav-item"><a href="#" class="nav-link">Contact</a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <section class="hero-wrap d-flex js-fullheight">
    	<div class="forth js-fullheight d-flex align-items-center">
    		<div class="text">
    			<span class="subheading">Hello, Selamat datang di website</span>
    			<h1>SMK PUI JATINANGOR</h1>
    			<h2 class="mb-5">Computer Network Education Program</h2>
    			<!-- <p><a href="#" class="btn-custom py-3 pr-2">Contact Me</a></p> -->
    		</div>
    	</div>
    	<div class="third about-img js-fullheight" style="background-image: url(images/siswa1.jpg);">
    		<a href="https://vimeo.com/45830194" class="icon popup-vimeo d-flex justify-content-center align-items-center">
      		<span class="icon-play"></a>
      	</a>
    	</div>
    </section>

		<section class="ftco-section ftco-services">
			<div class="container">
				<div class="row justify-content-center mb-5 pb-3">
          <div class="col-lg-7 heading-section ftco-animate">
          	<span class="subheading">Kompetensi Keahlian</span>
            <h2 class="mb-4">Teknik Komputer Jaringan</h2>
            <p>SMK PUI Jatinangor berkomitmen untuk mengembangkan potensi peserta didik agar menjadi manusia yang beriman dan bertakwa kepada Tuhan Yang Maha Esa, berakhlak mulia, sehat, berilmu, cakap, kreatif, mandiri, dan menjadi warga negara yang demokratis serta bertanggung jawab.</p>
          </div>
        </div>
		<div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services">
              <div class="media-body">
                <h3 class="heading">Program Tahsin & Tahfidz Al-Qur'an</h3>
                <p>Semua siswa akan diajarkan membaca dan menghapal Al Qur'an</p>
              </div>
            </div>      
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services">
              <div class="media-body">
                <h3 class="heading">Pengembangan Kurikulum Keagamaan</h3>
                <p>Pengembangan kurikulum dan pembinaan keagamaan Islam</p>
              </div>
            </div>    
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services">
              <div class="media-body">
                <h3 class="heading">Pengembangan Skill Teknologi</h3>
                <p>Pengembangan skill teknologi khususnya dalam bidang komputer dan jaringan</p>
              </div>
            </div>      
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services">
              <div class="media-body">
                <h3 class="heading">Ekstrakulilkuler</h3>
                <p>Pengembangan bakat non akademis siswa sesuai dengan minat bakat masing masing</p>
              </div>
            </div>      
          </div>
        </div>
			</div>
		</section>


		<section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url(images/siswa2.jpg);" data-stellar-background-ratio="0.5">
    	<div class="container">
    		<div class="row d-md-flex align-items-center">
    			<div class="col-lg-4">
    				<div class="heading-section pl-md-5 heading-section-white">
	          	<div class="ftco-animate">
		          	<span class="subheading"></span>
		            <h2 class="mb-4"></h2>
	            </div>
	          </div>
    			</div>
    			<!-- <div class="col-lg-8">
    				<div class="row d-md-flex align-items-center">
		          <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="3500">0</strong>
		                <span>Trusted clients</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="2000">0</strong>
		                <span>Solved Cases</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="800">0</strong>
		                <span>Awards Win</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="2450">0</strong>
		                <span>Winning Case</span>
		              </div>
		            </div>
		          </div>
	          </div> -->
          </div>
        </div>
    	</div>
	</section>
	
    <section class="ftco-section testimony-section">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section ftco-animate text-center">
          	<span class="subheading"></span>
            <h2 class="mb-4">Website saat ini masih dalam proses pengembangan</h2>
            <p></p>
          </div>
        </div>
      </div>
	</section>
	
    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
		  </div>
		<div class="col-md-4 text-center">
			<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
				<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
				<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
				<li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		  	</ul>
		</div>
		  
        </div>
      </div>
    </footer>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>